from django.urls import re_path

import app.views as views

app_name = 'app'

urlpatterns = [
    re_path(r'^start/$', views.StartEventAPIView.as_view(), name="start_event"),
    re_path(r'^finish/$', views.FinishEventAPIView.as_view(), name="finish_event"),
]
