from django.test import TestCase
from django.urls import reverse

from app.models import Event


class StartEventAPITestClass(TestCase):
    url = reverse('app:start_event')

    def setUp(self):
        event_active = Event.objects.create(type='event0active', state=Event.STATES.ACTIVE)
        event_finished = Event.objects.create(type='event0finished', state=Event.STATES.FINISHED)

    def test_start_event_of_new_type_ok(self):
        post_data = {'type': 'new1type2of3event'}
        r = self.client.post(self.url, data=post_data)
        self.assertEqual(r.status_code, 200)
        self.assertEqual(Event.objects.active().filter(type='new1type2of3event').count(), 1)

    def test_start_event_already_started_ok(self):
        self.assertEqual(Event.objects.active().filter(type='event0active').count(), 1)
        post_data = {'type': 'event0active'}
        r = self.client.post(self.url, data=post_data)
        self.assertEqual(r.status_code, 200)
        self.assertEqual(Event.objects.active().filter(type='event0active').count(), 1)

    def test_start_event_finished_ok(self):
        self.assertEqual(Event.objects.filter(type='event0finished').count(), 1)
        self.assertEqual(Event.objects.active().filter(type='event0finished').count(), 0)
        post_data = {'type': 'event0finished'}
        r = self.client.post(self.url, data=post_data)
        self.assertEqual(r.status_code, 200)
        self.assertEqual(Event.objects.active().filter(type='event0finished').count(), 1)
        self.assertEqual(Event.objects.filter(type='event0finished').count(), 2)

    def test_start_event_without_type_fail(self):
        r = self.client.post(self.url)
        self.assertEqual(r.status_code, 400)

    def test_start_event_invalid_type_parameter_fail(self):
        post_data = {'type': '_?djhfs'}
        r = self.client.post(self.url, data=post_data)
        self.assertEqual(r.status_code, 400)


class FinishEventAPITestClass(TestCase):
    url = reverse('app:finish_event')

    def setUp(self):
        event_active = Event.objects.create(type='event0active', state=Event.STATES.ACTIVE)
        event_finished = Event.objects.create(type='event0finished', state=Event.STATES.FINISHED)

    def test_finish_active_event_ok(self):
        self.assertEqual(Event.objects.active().filter(type='event0active').count(), 1)
        self.assertEqual(Event.objects.active().get(type='event0active').finished_at, None)
        post_data = {'type': 'event0active'}
        r = self.client.post(self.url, data=post_data)
        self.assertEqual(r.status_code, 200)
        self.assertEqual(Event.objects.active().filter(type='event0active').count(), 0)
        self.assertNotEqual(Event.objects.get(type='event0active').finished_at, None)

    def test_finish_finished_event_fail(self):
        self.assertEqual(Event.objects.filter(type='event0finished').count(), 1)
        self.assertEqual(Event.objects.active().filter(type='event0finished').count(), 0)
        post_data = {'type': 'event0finished'}
        r = self.client.post(self.url, data=post_data)
        self.assertEqual(r.status_code, 404)

    def test_finish_not_exist_event_fail(self):
        post_data = {'type': 'unknownevent'}
        r = self.client.post(self.url, data=post_data)
        self.assertEqual(r.status_code, 404)

