from django.db import models

STATE_ACTIVE = 0
STATE_FINISHED = 1


class EventQuerySet(models.query.QuerySet):

    def active(self):
        return self.filter(state = STATE_ACTIVE)

    def finished(self):
        return self.filter(state=STATE_FINISHED)
