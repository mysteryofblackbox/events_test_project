import datetime

from djongo import models
from app.managers import STATE_ACTIVE, STATE_FINISHED, EventQuerySet

class Event(models.Model):
    class STATES:
        ACTIVE=STATE_ACTIVE
        FINISHED=STATE_FINISHED
        CHOICES = (
            (ACTIVE, 'event is not finished'),
            (FINISHED, 'event is finished')
        )
    type = models.CharField(max_length=100)
    state = models.IntegerField(choices=STATES.CHOICES, default=STATES.ACTIVE)
    started_at = models.DateTimeField(auto_now_add=True)
    finished_at = models.DateTimeField(null=True, blank=True)
    objects = EventQuerySet.as_manager()

    def finish(self):
        self.state = self.STATES.FINISHED
        self.finished_at = datetime.datetime.now()
        self.save()
