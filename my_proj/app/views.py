import datetime
import re

from rest_framework import permissions, status
from rest_framework.views import APIView
from rest_framework.decorators import permission_classes
from rest_framework.response import Response

from django.shortcuts import get_object_or_404

from app.models import Event


class StartEventAPIView(APIView):
    permission_classes = (permissions.AllowAny,)

    def post(self, request):
        event_type = request.data.get('type')
        rule = re.compile(r'^[a-z0-9]+$')
        if event_type and rule.search(event_type):
            Event.objects.get_or_create(type=event_type, state=Event.STATES.ACTIVE)
            return Response(status=status.HTTP_200_OK)
        else:
            return Response('Valid type parameter is required', status=status.HTTP_400_BAD_REQUEST)


class FinishEventAPIView(APIView):
    permission_classes = (permissions.AllowAny,)

    def post(self, request):
        event_type = request.data.get('type')
        event = get_object_or_404(Event, state=Event.STATES.ACTIVE, type=event_type)
        event.finish()
        return Response(status=status.HTTP_200_OK)


