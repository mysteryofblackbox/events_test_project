from django.conf.urls import url, include
from django.urls import path

urlpatterns = [
    url(r'^v1/', include('app.urls', namespace='app')),
]
